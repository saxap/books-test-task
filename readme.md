Сборка и запуск:
<pre>
docker-compose up --build -d
./composer.sh install
./console.sh d:m:m
</pre>

Примеры запросов можно посмотреть в api/rest-tests

Запуск тестов:
<pre>
./run-test.sh
</pre>
<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class RestTest extends ApiTestCase
{
    const RU_BOOK_NAME = 'Книжка 1';

    public function testRest()
    {
        $authorId = $this->creatingAuthor();
        $bookId = $this->creatingBook($authorId);
        $this->errorCreatingAuthor();
        $this->searchBookByName();
        $this->getTranslatedBook($bookId, $authorId);
    }

    private function creatingAuthor(): string
    {
        $name = 'Автор 3000';

        $data = [
            'name' => $name,
        ];

        $response = static::createClient()->request('POST', '/author/save', [
                'json' => $data
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'success' => true,
            'data' => [
                'name' => $name
            ]
        ]);

        return $response->toArray()['data']['id'];
    }

    private function creatingBook($authorId)
    {
        $nameEn = 'Book 1';

        $data = [
            "authors" => [["id" => $authorId]],
            "translations" => [
                [
                    "name" => self::RU_BOOK_NAME,
                    "locale" => "ru"
                ],
                [
                    "name" => $nameEn,
                    "locale" => "en"
                ]
            ]
        ];

        $response = static::createClient()->request('POST', '/book/save', [
                'json' => $data
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'success' => true,
        ]);
        $this->assertJsonContains([
            'data' => [
                'translations' => [
                    'ru' => ['name' => self::RU_BOOK_NAME],
                    'en' => ['name' => $nameEn]
                ]
            ],
        ]);

        $responseData = $response->toArray();

        $this->assertCount(2, $responseData['data']['translations']);

        return $responseData['data']['id'];
    }

    private function errorCreatingAuthor()
    {
        $data = [
            'name' => '',
        ];

        $response = static::createClient()->request('POST', '/author/save', [
                'json' => $data
            ]
        );

        $this->assertResponseStatusCodeSame(400);
    }

    private function searchBookByName()
    {
        $response = static::createClient()->request('GET', '/book/search?name='.self::RU_BOOK_NAME);

        $this->assertResponseIsSuccessful();
        $responseData = $response->toArray();
        $this->assertCount(1, $responseData['data'] );
    }

    private function getTranslatedBook($bookId, $authorId)
    {
        $response = static::createClient()->request('GET', "/ru/book/$bookId");

        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'data' => [
                "authors" => [["id" => $authorId]],
                "name" => self::RU_BOOK_NAME
            ],
        ]);
    }
}

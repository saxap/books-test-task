<?php
namespace App\Service;

use App\Dto\AuthorDto;
use App\Dto\BookDto;
use App\Entity\Author;
use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Uid\Uuid;

class RestService
{
    private AuthorRepository $authorRepository;

    private BookRepository $bookRepository;

    private EntityManagerInterface $entityManager;

    public function __construct(
        AuthorRepository $authorRepository,
        BookRepository $bookRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->authorRepository = $authorRepository;
        $this->bookRepository = $bookRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getAuthorById(Uuid $uuid): Author
    {
        $author = $this->authorRepository->find($uuid);

        if (!$author) {
            throw new EntityNotFoundException('Автор не найден');
        }

        return $author;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getBookById(Uuid $uuid): Book
    {
        $book = $this->bookRepository->find($uuid);

        if (!$book) {
            throw new EntityNotFoundException('Книга не найдена');
        }

        return $book;
    }

    public function saveAuthorFromDto(AuthorDto $authorDto)
    {
        if ($authorDto->id) {
            $author = $this->getAuthorById( $authorDto->id );
        } else {
            $author = new Author();
        }

        $author->setName( $authorDto->name );
        $this->entityManager->persist($author);
        $this->entityManager->flush();

        return $author;
    }

    public function saveBookFromDto(BookDto $bookDto): Book
    {
        if ($bookDto->id) {
            $book = $this->getBookById( $bookDto->id );
        } else {
            $book = new Book();
        }

        foreach ($bookDto->authors as $authorDto) {
            $book->addAuthor( $this->getAuthorById($authorDto->id) );
        }

        foreach ($bookDto->translations as $translation) {
            $book->translate($translation->locale)->setName($translation->name);
        }

        $this->entityManager->persist($book);

        $book->mergeNewTranslations();

        $this->entityManager->flush();

        return $book;
    }

    public function searchBookByName(string $name)
    {
        return $this->bookRepository->searchByName($name);
    }
}
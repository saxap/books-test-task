<?php

namespace App\Serializer;

use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Uid\Uuid;

class UuidDenormalizer implements ContextAwareDenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return Uuid::fromString($data);
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return $type === Uuid::class;
    }
}

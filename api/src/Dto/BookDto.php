<?php
namespace App\Dto;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\BookTranslation;
use Symfony\Component\Uid\Uuid as Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class BookDto implements SimpleDto
{
    public ?Uuid $id = null;

    /**
     * @var AuthorDto[]
     * @Assert\NotBlank()
     */
    public array $authors = [];

    /**
     * @var BookTranslationDto[]
     * @Assert\NotBlank()
     */
    public array $translations = [];

    static function make(Book $book): self
    {
        $dto = new self();
        $dto->id = $book->getId();
        $dto->authors = array_map( fn(Author $author) => AuthorDto::make($author), $book->getAuthors()->toArray());
        $dto->translations = array_map( fn(BookTranslation $bookTranslation) => BookTranslationDto::make($bookTranslation), $book->getTranslations()->toArray());
        return $dto;
    }
}
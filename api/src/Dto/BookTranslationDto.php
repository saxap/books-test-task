<?php
namespace App\Dto;

use App\Entity\BookTranslation;
use Symfony\Component\Uid\Uuid as Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class BookTranslationDto implements SimpleDto
{
    public ?Uuid $id = null;

    /**
     * @Assert\NotBlank()
     */
    public ?string $name = null;

    /**
     * @Assert\NotBlank()
     */
    public ?string $locale = null; // мб тип какойта есть?

    static function make(BookTranslation $bookTranslation) : self
    {
        $dto = new self();
        $dto->id = $bookTranslation->getId();
        $dto->name = $bookTranslation->getName();
        $dto->locale = $bookTranslation->getLocale();
        return $dto;
    }
}
<?php
namespace App\Dto;

use App\Entity\Author;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class AuthorDto implements SimpleDto
{
    public ?Uuid $id = null;

    /**
     * @Assert\NotBlank()
     */
    public ?string $name = null;

    static function make(Author $author): self
    {
        $dto = new self();
        $dto->id = $author->getId();
        $dto->name = $author->getName();
        return $dto;
    }
}
<?php
namespace App\Dto;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\BookTranslation;
use Symfony\Component\Uid\Uuid as Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class TranslatedBookDto
{
    public Uuid $id;

    public string $name;

    public string $locale; // мб тип какойта есть?

    public array $authors = [];

    static function make(Book $book, string $lang): self
    {
        $dto = new self();
        $dto->id = $book->getId();
        $dto->authors = array_map( fn(Author $author) => AuthorDto::make($author), $book->getAuthors()->toArray());
        $dto->name = $book->translate( $lang )->getName();
        $dto->locale = $book->translate( $lang )->getLocale();
        return $dto;
    }
}
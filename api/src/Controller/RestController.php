<?php

namespace App\Controller;

use App\Dto\AuthorDto;
use App\Dto\BookDto;
use App\Dto\TranslatedBookDto;
use App\Entity\Book;
use App\Service\RestService;
use http\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

class RestController extends AbstractController
{
    const POSSIBLE_LANGS = ['en', 'ru'];

    private SerializerInterface $serializer;

    private RestService $restService;

    public function __construct(
        SerializerInterface $serializer,
        RestService $restService
    )
    {
        $this->serializer = $serializer;
        $this->restService = $restService;
    }



    /**
     * @Route("/{lang}/book/{id}", name="getBookByLangAndId", methods={"GET"})
     */
    public function getBookByLangAndId(string $lang, string $id): JsonResponse
    {
        if (!in_array($lang, self::POSSIBLE_LANGS)) { // такое конечно
            throw new InvalidArgumentException('Нет такого языка');
        }

        $book = $this->restService->getBookById( Uuid::fromString($id) );

        $dto = TranslatedBookDto::make($book, $lang);
        return $this->response($dto);
    }

    /**
     * @Route("/author/save", name="saveAuthor", methods={"POST", "PUT"})
     */
    public function saveAuthorFromDto(AuthorDto $authorDto): JsonResponse
    {
        $author = $this->restService->saveAuthorFromDto($authorDto);
        $dto = AuthorDto::make($author);
        return $this->response($dto);
    }

    /**
     * @Route("/book/save", name="saveBook", methods={"POST", "PUT"})
     */
    public function saveBookFromDto(BookDto $bookDto): JsonResponse
    {
        $book = $this->restService->saveBookFromDto($bookDto);
        $dto = BookDto::make($book);
        return $this->response($dto);
    }

    /**
     * @Route("/book/search", name="searchBooks", methods={"GET"})
     */
    public function searchBookByName(Request $request): JsonResponse
    {
        $books = $this->restService->searchBookByName( $request->query->get('name') );
        $bookDtos = array_map( fn(Book $book) => BookDto::make($book), $books );
        return $this->response($bookDtos);
    }

    /**
     * @Route("/author/{id}", name="getAuthorById", methods={"GET"})
     */
    public function getAuthorById(string $id): JsonResponse
    {
        $author = $this->restService->getAuthorById( Uuid::fromString($id) );
        $dto = AuthorDto::make($author);
        return $this->response($dto);
    }

    /**
     * @Route("/book/{id}", name="getBookById", methods={"GET"})
     */
    public function getBookById(string $id): JsonResponse
    {
        $book = $this->restService->getBookById( Uuid::fromString($id) );
        $dto = BookDto::make($book);
        return $this->response($dto);
    }



    private function response($object, $params = []): JsonResponse
    {
        $data = [
            'success' => true,
            'data' => $object
        ];

        $params = array_merge([], $params);

        $normalized = $this->serializer->normalize($data, 'json', $params);
        return new JsonResponse($normalized);
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Faker\Factory;
use Symfony\Component\Uid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220205155028 extends AbstractMigration
{
    const COUNT = 10000;

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $faker = Factory::create('ru_RU');
        $this->addSql("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";");
        for ($i = 0; $i < self::COUNT; $i++) {

            $authorName = $faker->name;
            $bookName = $faker->title;
            $bookNameEng = $this->translit($bookName);

            $bookId = Uuid::v4();
            $authorId = Uuid::v4();

            $this->addSql("INSERT INTO author (id, name) VALUES ('$authorId', '$authorName')");
            $this->addSql("INSERT INTO book (id) VALUES ('$bookId')");
            $this->addSql("INSERT INTO book_author (author_id, book_id) VALUES ('$authorId', '$bookId')");

            $this->addSql("INSERT INTO book_translation (id, translatable_id, name, locale) VALUES (uuid_generate_v1(), '$bookId', '$bookName', 'ru')");
            $this->addSql("INSERT INTO book_translation (id, translatable_id, name, locale) VALUES (uuid_generate_v1(), '$bookId', '$bookNameEng', 'en')");

        }

    }

    public function down(Schema $schema): void
    {
        $this->addSql("TRUNCATE book_author");
        $this->addSql("TRUNCATE book_translation");
        $this->addSql("TRUNCATE author");
        $this->addSql("TRUNCATE book");
    }

    private function translit($str): string
    {
        return iconv("UTF-8", "ISO-8859-1//TRANSLIT", $str);
    }
}
